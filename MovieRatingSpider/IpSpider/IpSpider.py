import requests
from lxml import etree
import time
import random


def getPage(tree):
    ipList = []
    portList = []
    for i in range(2, 14):
        ipxpath = '//*[@id="main"]/div/div[1]/table//tr[' + str(i) + ']/td[1]/text()'
        portxpath = '//*[@id="main"]/div/div[1]/table//tr[' + str(i) + ']/td[2]/text()'
        print(tree.xpath(ipxpath))
        if not tree.xpath(ipxpath):
            break
        else:
            ipList.append(tree.xpath(ipxpath)[0])
            portList.append(tree.xpath(portxpath)[0])
    return dict(zip(ipList, portList))


def ipSpider():
    ipdict = {}
    html = requests.get('http://www.66ip.cn/').content.decode('gbk', 'ignore')
    # print(html)
    tree = etree.HTML(html)
    path = '//*[@id="PageList"]/a[12]/text()'
    num = tree.xpath(path)[0]
    print(num)
    startpage = 12
    endpage = 40
    for page in range(startpage, endpage):
        url = 'http://www.66ip.cn/' + str(page) + '.html'
        print(url)
        # response = requests.get(url).text.encode('utf8', 'ignore')
        html = requests.get(url).content.decode('gbk', 'ignore')
        tree = etree.HTML(html)
        onepage_resulte = getPage(tree)
        if onepage_resulte:
            ipdict = dict(ipdict, **onepage_resulte)
        time.sleep(random.random() * 3)
    return ipdict, endpage - startpage + 2



def main():
    IPList_resulte = ipSpider()
    # print(IPList_resulte)


if __name__ == '__main__':
    main()
