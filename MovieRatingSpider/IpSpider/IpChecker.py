# 检查IP是否可用部分
import requests
from MysqlApi import get_connection, insert


def checking(ipdict):
    '''
    :param ipdict: a dict like {'ipxxx':'portxxx','167.71.2.12':'11111'}
    :return: None
    '''
    proxies = []
    for ip in ipdict.keys():
        theProxy = 'http://' + ip + ':' + ipdict[ip]
        proxies.append(theProxy)
        IP = theProxy

        get_connection()
        res = requests.get("https://movie.douban.com/", proxies={"http": IP},
                           headers={'Host': 'movie.douban.com',
                                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0',
                                    },timeout=8)
        # print(res.content)
        print(res.status_code)
        if res.status_code==200:
            get_connection()
            sql = 'insert into internet(ip, port, id) values(%s, %s, default)'
            insert(sql, [ip, ipdict[ip]])
            result = 'success'
            print("success")
        else:
            print("fail")
