from multiprocessing.pool import Pool
from time import sleep
import requests
import re
from MysqlApi import get_connection, select_all, insert

headers = {'Host': 'movie.douban.com',
           'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0',
           }



def getUserRate(start,end,ip,userid):
    sql = 'select * from test.username where id>'+str(start)+' and id<'+str(end)
    # print(sql)
    AllIp = select_all(sql)
    # print(all)
    for pair in AllIp:
        print('id : ' + str(pair['id']))
        if pair['id'] <= 2: continue;
        url = 'https://movie.douban.com/people/' + pair[
            'name'] + '/collect?sort=time&amp;start=0&amp;filter=all&amp;mode=list&amp;tags_sort=count'
        html = requests.get(url, headers=headers,proxies={"http": ip}).text
        # print(html)
        page_pattern = '.*?data-total-page="(.*?)".*?'
        pages = re.match(page_pattern, html, re.S)
        # print(pages)
        if pages == None:
            continue
        else:
            pages = int(pages.group(1))
            print("pages is", pages)
            userid += 1

        for page in range(pages):
            print(page)
            url = 'https://movie.douban.com/people/' + pair[
                'name'] + '/collect?sort=time&amp;start=' + str(
                page * 30) + '&amp;filter=alssl&amp;mode=list&amp;tags_sort=count'
            print(url)
            html = requests.get(url, headers=headers,proxies={"http": ip}).text
            moive_pattern = '.*?<ul class="list-view">(.*?)</ul>.*?'
            moives = re.match(moive_pattern, html, re.S)
            if moives:
                moives = moives.group(1)
                # print(moives)
            else:
                print('continue')
                continue
            # print(moives)
            rate_pattern = '''                    <a href="https://movie.douban.com/subject/(.*?)/">
                        (.*?)
                    </a>.*?
                </div>.*?
                <div class="date">.*?
                        <span class="rating(.*?)-t"></span>&nbsp;&nbsp;'''
            result = re.findall(rate_pattern, moives, re.S)
            print('result is',result)
            if result:
                for rate in result:
                    sql = 'insert into rate(userid,username,moiveid,rate) values(%s, %s, %s, %s)'
                    insert(sql, [userid,pair['name'],rate[0],rate[2]])
            sleep(1.5)
    print('finish id:',str(pair['id']))


if __name__=="__main__":
    sql = 'select * from test.internet'
    all = select_all(sql)
    ip = ['http://'+str(all[i]['ip'])+':'+str(all[i]['port']) for i in range(len(all))]
    ip_count=ip.count();
    # argumentslist = [(i * 10, (i + 1) * 10, ip[(i - 26) % 250],(27+i-35)*10) for i in range(35, 525)]
    argumentslist = [(i * 10, (i + 1) * 10, ip[i % ip_count],i*10) for i in range(0, 525)]
    with Pool() as pool:
        pool.starmap(getUserRate,argumentslist)
