import pymysql

MYSQL_DB = 'test'
MYSQL_USER = 'root'
MYSQL_PASS = '13021910302'
MYSQL_HOST = '127.0.0.1'

def get_connection():
    """连接数据库"""
    # 连接数据库
    connect = pymysql.Connect(
        host=MYSQL_HOST,
        port=3306,
        user=MYSQL_USER,
        passwd=MYSQL_PASS,
        db=MYSQL_DB,
        charset='utf8'
    )
    # 获取游标(指定获取的数据格式，这里设定返回dict格式)
    return connect, connect.cursor(cursor=pymysql.cursors.DictCursor)


def select_all(sql, args=None):
    """查询所有"""
    conn, cursor = get_connection()
    cursor.execute(sql, args)
    results = cursor.fetchall()
    cursor.close()
    conn.close()
    return results


def select_one(sql, args=None):
    """查询一个"""
    conn, cursor = get_connection()
    cursor.execute(sql, args)
    result = cursor.fetchone()
    cursor.close()
    conn.close()
    return result


def update(sql, args):
    """修改数据"""
    conn, cursor = get_connection()
    cursor.execute(sql, args)
    conn.commit()
    cursor.close()
    conn.close()


def insert(sql, args=None):
    """新增数据"""
    conn, cursor = get_connection()
    cursor.execute(sql, args)
    conn.commit()
    cursor.close()
    conn.close()


def delete(sql, args=None):
    """删除数据"""
    conn, cursor = get_connection()
    cursor.execute(sql, args)
    conn.commit()
    cursor.close()
    conn.close()


