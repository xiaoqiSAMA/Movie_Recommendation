#编码格式
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

from pyspark import SparkContext,SparkConf
from pyspark.sql import SparkSession

conf = SparkConf().setAppName('demo').setMaster('spark://master:7077')
sc = SparkContext(conf=conf)

#从hdfs上直接读取csv文件
spark = SparkSession(sc)
# print('------------------------------------开始读取文件---------------------------------------')
all_ratings = spark.read.csv('hdfs://master:9000/user/root/movie_ratings.csv', sep = ',', header = True)
# print('------------------------------------读取文件完成---------------------------------------')

#通过hive读取hdfs中的数据
# from pyspark.sql import SparkSession,HiveContext
# spark_host = "spark://master:7077"
# name = "test"
# spark_session = SparkSession.builder.master(spark_host).appName(name).getOrCreate()
# hive_context = HiveContext(spark_session)
# hive_database = ""          #hive的数据库名称
# hive_table = ""             #hive的表名
# hive_read = f"select * from {hive_database}.{hive_table}"
# all_ratings = hive_context.sql(hive_read)

# ratings = userid=1~200的数据
all_ratings = all_ratings.filter(all_ratings.userid <= 200)

#处理脏数据
usernames = set(all_ratings.select(all_ratings['username']).collect())
maps = dict()
count = 1
for i in usernames:
    maps[i['username']] = count
    count += 1

from pyspark.sql.functions import udf
from pyspark.sql.types import IntegerType

def dirty(name):
    return maps[name]
udfname = udf(dirty, IntegerType())

all_ratings = all_ratings.withColumn('userid',udfname(all_ratings.username))

#通过userid列排序
all_ratings = all_ratings.orderBy('userid')

#丢掉username列
all_ratings = all_ratings.drop('username')

# 新设一列'喜爱' 把Rating大于3的设为True
all_ratings = all_ratings.withColumn('Favorable',all_ratings.rate > 3)


# 在ratings中Favorable为True的数据
favorable_ratings = all_ratings.filter(all_ratings.Favorable == False)

# 仅提取那些有多个电影评论的用户id
#数据格式: {userid : frozenset(movies' id)}
favorable = favorable_ratings.select('userid','moiveid').collect()


favorable_reviews_by_users = dict()
temp = []
for num in range(1,200):
    for row in favorable:
        if row['userid']==num:
            temp.append(int(row['moiveid'])) 
    favorable_reviews_by_users[num] = frozenset(temp)
    temp = []

# 求出每部电影的'喜爱'的数量
favorable_count = favorable_ratings.select('moiveid','Favorable').collect()

from collections import defaultdict
favorable = defaultdict(int)
for i in favorable_count:
    favorable[int(i['moiveid'])] += 1

# 把favorable数据存为 favorable.csv 文件用于可视化
# impor csv
# with open('favorable.csv','w') as f:
#     writer = csv.writer(f)
#     writer.writerrows(favorable.items())


data = [(k,v) for k,v in favorable.items()]
num_favorable_by_movie = spark.createDataFrame(data, schema=['movieid','favorableNum'])

#迭代更新频繁项集
def find_frequent_itemsets(favorable_reviews_by_users, k_1_itemsets, min_support):
    '''
    favorable_reviews_by_users: 数据格式: {userid : frozenset(movies id)} 
    k_1_itemsets: k=1的频繁项集
    min_support: 最小支持度
    '''
    counts = defaultdict(int)
    for user, reviews in favorable_reviews_by_users.items():
        for itemset in k_1_itemsets:
            #对每个userid取其中所有的movies id与频繁项集中的热门电影的movie id进行判断        
            if itemset.issubset(reviews):
                for other_reviewed_movie in reviews - itemset:
                    #把movies id中去除热门movie id与热门的movie id进行关联
                    #current_superset: 数据格式 { , itemset,} -> 更新为k=2的项集
                    current_superset = itemset | frozenset((other_reviewed_movie,))
                    #统计频率
                    counts[current_superset] += 1
    #return的数据格式: { {other_movieid, Favorable_movieid,} : num_of_frequency) } -> k=2 itemsets的长度也为2
    return dict([(itemset, frequency) for itemset, frequency in counts.items() if frequency >= min_support])


import sys
frequent_itemsets = {}  # 频繁项集
min_support = 75        #最小支持度
# print('------------------------------------更新频繁项集---------------------------------------')
# 构建k=1的频繁项集 {1 : {set((movieid,)) : num_of_Favorable}}
frequent_itemsets[1] = dict((frozenset((row['movieid'],)), row["favorableNum"])
                                for row in num_favorable_by_movie.collect()
                                if row["favorableNum"] > min_support)
# print(frequent_itemsets[1])
print("总共有 {} 部电影超过 {} 人的喜爱".format(len(frequent_itemsets[1]), min_support))
sys.stdout.flush()
for k in range(2, 20):
    # k的迭代用于迭代更新频繁项集的length
    # 相比于k=1的频繁项集 cur_frequent_itemsets的 set((movieid,))的length为2
    # 在迭代过程中 求得k个movieid的关联的频率 存入频繁项集中
    cur_frequent_itemsets = find_frequent_itemsets(favorable_reviews_by_users, frequent_itemsets[k-1],min_support)
    if len(cur_frequent_itemsets) == 0:
        with open('info.txt', 'a') as f:
            f.write("未找到length为{}的关联项集".format(k))
        print("未找到length为{}的关联项集".format(k))
        sys.stdout.flush()
        break
    else:
        print("找到了{}个length为{}的关联项集".format(len(cur_frequent_itemsets), k))
        with open('info.txt', 'a') as f:
            f.write("找到了{}个length为{}的关联项集".format(len(cur_frequent_itemsets), k))
#         print(cur_frequent_itemsets)
        sys.stdout.flush()
        frequent_itemsets[k] = cur_frequent_itemsets
# 单独一个movie无关联 删去
del frequent_itemsets[1]

print("总共找到了{}个关联项集".format(sum(len(itemsets) for itemsets in frequent_itemsets.values())))

candidate_rules = []
for itemset_length, itemset_counts in frequent_itemsets.items():
    for itemset in itemset_counts.keys():
        for conclusion in itemset:
            premise = itemset - set((conclusion,))
            candidate_rules.append((premise, conclusion))

print("总共又 {} 条关联规则".format(len(candidate_rules)))
#candidate_rules : [ (movieid,{othoer_movieid,}) ]

correct_counts = defaultdict(int)
incorrect_counts = defaultdict(int)
#favorable_reviews_by_users : {userid : frozenset(movies' id)}
# print('------------------------------------更新关联规则---------------------------------------')
for user, reviews in favorable_reviews_by_users.items():
    for candidate_rule in candidate_rules:
        premise, conclusion = candidate_rule
        if premise.issubset(reviews):
            if conclusion in reviews:
                correct_counts[candidate_rule] += 1
            else:
                incorrect_counts[candidate_rule] += 1
rule_confidence = {candidate_rule: correct_counts[candidate_rule] / float(correct_counts[candidate_rule] + incorrect_counts[candidate_rule])
              for candidate_rule in candidate_rules if incorrect_counts[candidate_rule] != 0}
print(len(rule_confidence))

min_confidence = 0.9

rule_confidence = {rule: confidence for rule, confidence in rule_confidence.items() if confidence > min_confidence}

# 按照置信度排序
# from operator import itemgetter
# sorted_confidence = sorted(rule_confidence.items(), key=itemgetter(1), reverse=True)


#movieid转name和year的api
# import requests
# from bs4 import BeautifulSoup
# def get_html(url,headers):
#     r = requests.get(url,headers=headers).text
#     return r

# def movie_info(movieid):
#     headers = {
#     'Cookies' : '"__utma":"223695111.1924845966.1576487347.1580290450.1582339105.3","__utmb":"223695111.0.10.1582339105","__utmc":"223695111","__utmt":"1","__utmz":"223695111.1582339105.3.3.utmcsr=douban.com|utmccn=(referral)|utmcmd=referral|utmcct=/","__yadk_uid":"xKZ3DNC6OVdDWei9jMvn1uIKTdo63iXA","_pk_id.100001.4cf6":"3bbe27fdf5e1e11b.1576487347.3.1582339128.1580290484.","_pk_ref.100001.4cf6":"[\"\",\"\",1582339106,\"https://www.douban.com/\"]","_pk_ses.100001.4cf6":"*","_vwo_uuid_v2":"D9C17EE42BBCDA73D478195EAF81C9E00|d77dd05d5e635dc6a664bc3a2a25279d","ap_v":"0,6.0","bid":"bWwC_8X-XGU","ll":"\"108296\"',
#     'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363',
#     'Host': 'movie.douban.com'
# }
#     url = 'https://movie.douban.com/subject/{}/'.format(movieid)
#     r = get_html(url,headers)
#     soup = BeautifulSoup(r,'lxml')
#     name = soup.find('span',attrs={'property':'v:itemreviewed'})
#     year = soup.find('span',attrs={'class':'year'})
#     return "{} ({})".format(name.text,year.text)


# 写成文件用于可视化
with open('movie_info.csv','w') as f:
    for item,confid in rule_confidence:
        # 展示数据
        # print("Rule #{0}".format(index + 1))
        # (premise, conclusion) = sorted_confidence[index][0]
        # conc_name = movie_info(conclusion)
        # prem_names = ','.join(movie_info(i) for i in premise)
        # print("Rule: 如果有人喜欢 {0} 那他们也可能喜欢 {1}".format(prem_names, conc_name))
        # print(" - 置信度: {0:.3f}".format(rule_confidence[(premise, conclusion)]))
        # print("")

        
        f.write(str(item[1])+'#'+str(item[0])+'#'+str(confid)+'\n')
        
