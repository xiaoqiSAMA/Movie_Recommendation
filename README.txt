1.爬虫文件需先新建数据库
在本地mysql创建test数据库，并创建internet，rate两个表。
CREATE TABLE `internet` (
  `ip` varchar(45) DEFAULT NULL,
  `port` varchar(45) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=604 DEFAULT CHARSET=utf8

CREATE TABLE `rate` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `moiveid` int(11) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`userid`,`moiveid`)
) ENGINE=InnoDB AUTO_INCREMENT=4682 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

更改项目下MysqlApi.py文件的MYSQL_DB， MYSQL_USER， MYSQL_PASS，  MYSQL_HOST四项配置的值
完成后运行IpSpider文件夹下的IpSpider.py文件,进行代理ip的爬取。
待上一步完成后运行RatingSpider文件夹下的RatingSpider.py即可。

并将数据写成文件存入HDFS中

2.使用spark-submit提交spark.py文件，默认参数为  --name demo  --master spark://master:7077
spark.py运行结果为 生成一个挖掘的关联数据csv文件到当前文件夹目录底下
